//
//  ReverseLongestName.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 9/30/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

extension Hero {
    
    func reverseLongestName(inventory: [UDItem]) -> String {
        var length = 0
        var i:Int = 0
        var long:String = ""
        var longRev:String = ""
        while(i<inventory.count){
            
            if (inventory[i].name.characters.count > length){
                length = inventory[i].name.characters.count
                long = inventory[i].name
            }
            i++
        }
        var longest = long.characters.reverse()
        longRev = String(longest)
        return longRev
    }
    
}

// If you have completed this function and it is working correctly, feel free to skip this part of the adventure by opening the "Under the Hood" folder, and making the following change in Settings.swift: "static var RequestsToSkip = 1"